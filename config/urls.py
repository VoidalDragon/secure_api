from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views import defaults as default_views
from django.views.generic import TemplateView
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework.authtoken.views import obtain_auth_token

from rest_framework.views import APIView
from rest_framework.response import Response

from oauth2_provider.contrib.rest_framework import OAuth2Authentication
from oauth2_provider.oauth2_backends import get_oauthlib_core

from oauthlib.common import Request
from oauthlib.oauth2.rfc6749.tokens import get_token_from_header
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope


class Auth(OAuth2Authentication):
    def authenticate(self, request):
        print("Auth.authenticate")
        core = get_oauthlib_core()
        print("Auth core:", core)
        # print("Request:", core.verify_request(request, scopes=[]))

        print(" ---- PARAMS ---- ")
        uri, http_method, body, headers = core._extract_params(request)
        print("URI:", uri)
        print("HTTP method:", http_method)
        print("Body:", body)
        print("Headers:", headers)
        print(" ---- END PARAMS ---- ")

        print("Server:", core.server)

        core.server.verify_request(uri, http_method, body, headers, scopes=[])
        oauth_req = Request(uri, http_method, body, headers)
        print("Token Type:", core.server.find_token_type(oauth_req))
        print(
            "Token Handler:",
            core.server.tokens.get(core.server.find_token_type(oauth_req), core.server.default_token_type_handler),
        )

        print(" ---- Handler ---- ")
        handler = core.server.tokens.get(
            core.server.find_token_type(oauth_req), core.server.default_token_type_handler
        )
        print(
            "Token Handler:",
            core.server.tokens.get(core.server.find_token_type(oauth_req), core.server.default_token_type_handler),
        )
        print("Token:", get_token_from_header(oauth_req))
        print("Token headers:", oauth_req.headers)

        return super().authenticate(request)


class SecretTeller(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """

    authentication_classes = [Auth]
    # permission_classes = [permissions.IsAdminUser]
    permission_classes = [TokenHasReadWriteScope]
    required_scopes = ["read"]

    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        return Response({"secret": "Santa is not real!"})


urlpatterns = [
    path("", TemplateView.as_view(template_name="pages/home.html"), name="home"),
    path("about/", TemplateView.as_view(template_name="pages/about.html"), name="about"),
    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL, admin.site.urls),
    # User management
    path("users/", include("secure_api.users.urls", namespace="users")),
    path("accounts/", include("allauth.urls")),
    # Your stuff: custom urls includes go here
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# API URLS
urlpatterns += [
    # API base url
    path("api/", include("config.api_router")),
    path("api/secret/", SecretTeller.as_view()),
    # DRF auth token
    path("auth-token/", obtain_auth_token),
    path("api/schema/", SpectacularAPIView.as_view(), name="api-schema"),
    path(
        "api/docs/",
        SpectacularSwaggerView.as_view(url_name="api-schema"),
        name="api-docs",
    ),
]

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
